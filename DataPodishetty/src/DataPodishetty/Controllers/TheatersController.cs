using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataPodishetty.Models;

namespace DataPodishetty.Controllers
{
    public class TheatersController : Controller
    {
        private AppDbContext _context;

        public TheatersController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Theaters
        public IActionResult Index()
        {
            var appDbContext = _context.Theaters.Include(t => t.Location);
            return View(appDbContext.ToList());
        }

        // GET: Theaters/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Theater theater = _context.Theaters.Single(m => m.TheatreId == id);
            if (theater == null)
            {
                return HttpNotFound();
            }

            return View(theater);
        }

        // GET: Theaters/Create
        public IActionResult Create()
        {
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location");
            return View();
        }

        // POST: Theaters/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Theater theater)
        {
            if (ModelState.IsValid)
            {
                _context.Theaters.Add(theater);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", theater.LocationID);
            return View(theater);
        }

        // GET: Theaters/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Theater theater = _context.Theaters.Single(m => m.TheatreId == id);
            if (theater == null)
            {
                return HttpNotFound();
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", theater.LocationID);
            return View(theater);
        }

        // POST: Theaters/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Theater theater)
        {
            if (ModelState.IsValid)
            {
                _context.Update(theater);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewData["LocationID"] = new SelectList(_context.Locations, "LocationID", "Location", theater.LocationID);
            return View(theater);
        }

        // GET: Theaters/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Theater theater = _context.Theaters.Single(m => m.TheatreId == id);
            if (theater == null)
            {
                return HttpNotFound();
            }

            return View(theater);
        }

        // POST: Theaters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Theater theater = _context.Theaters.Single(m => m.TheatreId == id);
            _context.Theaters.Remove(theater);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
