using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataPodishetty.Models;

namespace DataPodishetty.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160224221151_InitialMigration")]
    partial class InitialMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataPodishetty.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<int?>("TheaterTheatreId");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataPodishetty.Models.Theater", b =>
                {
                    b.Property<int>("TheatreId")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("LocationID");

                    b.Property<string>("ManagerName");

                    b.Property<string>("Moviename");

                    b.Property<string>("Theatername");

                    b.HasKey("TheatreId");
                });

            modelBuilder.Entity("DataPodishetty.Models.Location", b =>
                {
                    b.HasOne("DataPodishetty.Models.Theater")
                        .WithMany()
                        .HasForeignKey("TheaterTheatreId");
                });

            modelBuilder.Entity("DataPodishetty.Models.Theater", b =>
                {
                    b.HasOne("DataPodishetty.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
