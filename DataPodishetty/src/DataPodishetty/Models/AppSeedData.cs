﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DataPodishetty.Models
{
    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider,String appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();
           
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }

            context.Theaters.RemoveRange(context.Theaters);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);

            SeedTheatersFromCsv(relPath, context);


        }
        private static void SeedTheatersFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "theaters.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Theater.ReadAllFromCSV(source);
            List<Theater> lst = Theater.ReadAllFromCSV(source);
            context.Theaters.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
         
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}
