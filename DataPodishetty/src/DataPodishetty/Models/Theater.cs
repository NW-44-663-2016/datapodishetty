﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace DataPodishetty.Models
{
    public class Theater
    {
        [Key]
        [ScaffoldColumn(false)]
        public int TheatreId { get; set; }

        [Display(Name = "Theater Name")]
        public string Theatername { get; set; }

        [Display(Name = "Manager Name")]
        public string ManagerName { get; set; }

        [Display(Name = "Movie Name")]
        public string Moviename { get; set; }


        [ScaffoldColumn(true)]
        public int? LocationID { get; set; }

        public virtual Location Location { get; set; }


        // a list of all places the movie was filmed

        //public List<Location> filmSets { get; set; }

        public static List<Theater> ReadAllFromCSV(string filepath)
        {
            List<Theater> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Theater.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Theater OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Theater item = new Theater();

            int i = 0;
            item.Theatername = Convert.ToString(values[i++]);
            item.ManagerName = Convert.ToString(values[i++]);
            item.Moviename = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
